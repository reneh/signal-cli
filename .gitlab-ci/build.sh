#!/bin/bash

set -e

if [[ -n "${DEBUG}" ]]; then
    set -x
fi

export DEBIAN_FRONTEND=noninteractive
tag="${CI_COMMIT_TAG:-v0.0.0+0}"
version="${tag%%+*}"
patchlevel="${tag##*+}"
if [[ "${version}" =~ ^@.*$ ]]; then
    signal_cli_branch="${version/@/}"
fi
graalvm="${GRAALVM_VERSION:-22.3.3}"
java="${JAVA_VERSION:-java17}"
architecture="${ARCH:-amd64}"
apt-get -qq update
apt-get -qqy install \
    curl
case "${CI_JOB_NAME}" in
    "signal-cli-native"*|"signal-cli-jre"*)
        if [ ! -f /tmp/libsignal_jni.so ]; then
            if [ ! -d "/tmp/signal-cli-${version//v/}" ]; then
                curl -sfLo "/tmp/signal-cli-${version//v/}.tar.gz" \
                    "https://github.com/AsamK/signal-cli/releases/download/${version}/signal-cli-${version//v/}.tar.gz"
                tar -C /tmp -xzf "/tmp/signal-cli-${version//v/}.tar.gz"
            fi
            libsignalclient="$(find "/tmp/signal-cli-${version//v/}/lib" -type f -name 'libsignal-client-*.jar')"
            if [ -z "${libsignalclient}" ]; then
                printf "\e[1;32mERROR: unable to determine libsignal-client\e[0m\n"
                exit 1
            fi
            libsignalclient_version="${libsignalclient//.jar/}"
            libsignalclient_version="${libsignalclient_version##*-}"
            case "${architecture}" in
                "amd64")
                    libsignalclient_download_url="https://github.com/exquo/signal-libs-build/releases/download/libsignal_v${libsignalclient_version}/libsignal_jni.so-v${libsignalclient_version}-x86_64-unknown-linux-gnu.tar.gz"
                    ;;
                "arm64")
                    libsignalclient_download_url="https://github.com/exquo/signal-libs-build/releases/download/libsignal_v${libsignalclient_version}/libsignal_jni.so-v${libsignalclient_version}-aarch64-unknown-linux-gnu.tar.gz"
                    ;;
            esac
            curl -sfLo /tmp/libsignal_jni.so.tgz "${libsignalclient_download_url}"
            tar -C /tmp/ -xzf /tmp/libsignal_jni.so.tgz
        fi
        if ldd /tmp/libsignal_jni.so | grep --color=never -i "not found"; then
            printf "\e[1;32mERROR: current glibc does not fulfill all libsignal-client requirements\e[0m\n"
            exit 1
        fi
        ;;
esac
printf "\e[1;36mBuild configuration:\e[0m\n"
printf "\e[0;36mArchitecture: %s\e[0m\n" "${architecture}"
printf "\e[0;36mGraalVM (native builds): %s\e[0m\n" "${graalvm}"
printf "\e[0;36mJava: %s\e[0m\n" "${java}"
printf "\e[0;36mlibsignal-client: %s\e[0m\n" "${libsignalclient_version}"

apt-get -qqy install \
    ruby-dev \
    ruby-ffi \
    git \
    binutils \
    zip
type fpm >/dev/null 2>&1 || (
    gem install fpm --no-doc
)

tmpdir="$(mktemp -d)"

case "${NAME}" in
"signal-cli-native")
    apt-get -y install \
        build-essential \
        libz-dev
    description="signal-cli provides an unofficial commandline and dbus interface for signalapp/libsignal-service-java - native build"
    case "${architecture}" in
        "arm64")
            graalvm_arch="aarch64"
            ;;
    esac
    mkdir -p "${tmpdir}/usr/bin"
    if [ ! -d "/tmp/graalvm-ce-${java}-${graalvm}" ]; then
        curl -Lo "/tmp/graalvm-ce-${java}-linux-${graalvm_arch:-${architecture}}-${graalvm}.tar.gz" \
            "https://github.com/graalvm/graalvm-ce-builds/releases/download/vm-${graalvm}/graalvm-ce-${java}-linux-${graalvm_arch:-${architecture}}-${graalvm}.tar.gz"
        tar -C /tmp -xzf "/tmp/graalvm-ce-${java}-linux-${graalvm_arch:-${architecture}}-${graalvm}.tar.gz"
    fi
    export PATH="/tmp/graalvm-ce-${java}-${graalvm}/bin:${PATH}"
    export JAVA_HOME="/tmp/graalvm-ce-${java}-${graalvm}"
    if [ ! -d /tmp/signal-cli ]; then
        git clone -b "${signal_cli_branch:-${version}}" https://github.com/AsamK/signal-cli /tmp/signal-cli
    fi
    cd /tmp/signal-cli
    git reset --hard
    find "${CI_PROJECT_DIR}/.patches" -type f -name '*.diff' -print0 | xargs --null -L1 git apply
    if [ -n "${REFLECT_CONFIG}" ]; then
        curl -L "${REFLECT_CONFIG}" \
            >/tmp/signal-cli/graalvm-config-dir/reflect-config.json
    fi
    if [ ! -f lib/src/main/resources/libsignal_jni.so ]; then
        cp -v /tmp/libsignal_jni.so lib/src/main/resources/libsignal_jni.so
    fi
    if [ ! -f build/native/nativeCompile/signal-cli ]; then
        ./gradlew nativeCompile
    fi
    mkdir -p "${tmpdir}/usr/bin"
    cp -v build/native/nativeCompile/signal-cli "${tmpdir}/usr/bin/signal-cli-native"
    cd -
    sed 's,%BINARY%,signal-cli-native,' "${CI_PROJECT_DIR}/.packaging/template.after-install.sh" >/tmp/after-install.sh
    sed 's,%BINARY%,signal-cli-native,' "${CI_PROJECT_DIR}/.packaging/template.after-upgrade.sh" >/tmp/after-upgrade.sh
    sed 's,%BINARY%,signal-cli-native,' "${CI_PROJECT_DIR}/.packaging/template.before-remove.sh" >/tmp/before-remove.sh
    sed -i 's,%ORDER%,10,' /tmp/after-install.sh /tmp/after-upgrade.sh
    fpm_source_dirs="usr"
    fpm_opts="--after-install /tmp/after-install.sh"
    fpm_opts+=" --after-upgrade /tmp/after-upgrade.sh"
    fpm_opts+=" --before-remove /tmp/before-remove.sh"
    fpm_opts+=" --before-upgrade ${CI_PROJECT_DIR}/.packaging/before-upgrade.sh"
    fpm_opts+=" --provides signal-cli"
    fpm_opts+=" --deb-suggests sqlite3"
    ;;
"signal-cli-jre")
    description="signal-cli provides an unofficial commandline and dbus interface for signalapp/libsignal-service-java"
    zip -vj "${libsignalclient}" /tmp/libsignal_jni.so
    mkdir -p \
        "${tmpdir}/usr/share/signal-cli" \
        "${tmpdir}/usr/bin"
    cd "/tmp/signal-cli-${version//v/}"
    sed 's,^APP_HOME=.*exit$,APP_HOME=/usr/share/signal-cli,' bin/signal-cli >"${tmpdir}/usr/bin/signal-cli-jre"
    chmod +x "${tmpdir}/usr/bin/signal-cli-jre"
    cp -rv lib "${tmpdir}/usr/share/signal-cli/"
    cd -
    sed 's,%BINARY%,signal-cli-jre,' "${CI_PROJECT_DIR}/.packaging/template.after-install.sh" >/tmp/after-install.sh
    sed 's,%BINARY%,signal-cli-jre,' "${CI_PROJECT_DIR}/.packaging/template.after-upgrade.sh" >/tmp/after-upgrade.sh
    sed 's,%BINARY%,signal-cli-jre,' "${CI_PROJECT_DIR}/.packaging/template.before-remove.sh" >/tmp/before-remove.sh
    sed -i 's,%ORDER%,20,' /tmp/after-install.sh /tmp/after-upgrade.sh
    fpm_opts="--depends ${java}-runtime-headless"
    fpm_opts+=" --after-install /tmp/after-install.sh"
    fpm_opts+=" --after-upgrade /tmp/after-upgrade.sh"
    fpm_opts+=" --before-remove /tmp/before-remove.sh"
    fpm_opts+=" --before-upgrade ${CI_PROJECT_DIR}/.packaging/before-upgrade.sh"
    fpm_opts+=" --provides signal-cli"
    fpm_opts+=" --deb-suggests sqlite3"
    fpm_source_dirs="usr"
    ;;
"signal-cli-dbus-service")
    description="signal-cli provides an unofficial commandline and dbus interface for signalapp/libsignal-service-java - DBus system service"
    architecture="all"
    mkdir -p \
        "${tmpdir}/etc/dbus-1/system.d" \
        "${tmpdir}/usr/share/dbus-1/system-services" \
        "${tmpdir}/etc/default"
    cp "${CI_PROJECT_DIR}/.packaging/org.asamk.Signal.conf" "${tmpdir}/etc/dbus-1/system.d/org.asamk.Signal.conf"
    curl -Lo "${tmpdir}/usr/share/dbus-1/system-services/org.asamk.Signal.service" "https://raw.githubusercontent.com/AsamK/signal-cli/${version}/data/org.asamk.Signal.service"
    cp -v "${CI_PROJECT_DIR}/.packaging/default-signal-cli-dbus" "${tmpdir}/etc/default/signal-cli-dbus"
    fpm_source_dirs="etc usr"
    fpm_opts="--depends signal-cli"
    fpm_opts+=" --depends systemd"
    fpm_opts+=" --deb-suggests dbus"
    fpm_opts+=" --deb-systemd ${CI_PROJECT_DIR}/.packaging/signal-cli-dbus.service"
    fpm_opts+=" --after-install ${CI_PROJECT_DIR}/.packaging/dbus-service-after-install.sh"
    ;;
"signal-cli-service")
    description="signal-cli provides an unofficial commandline and dbus interface for signalapp/libsignal-service-java - system service"
    architecture="all"
    mkdir -p \
        "${tmpdir}/etc/default"
    cp -v "${CI_PROJECT_DIR}/.packaging/default-signal-cli" "${tmpdir}/etc/default/signal-cli"
    fpm_source_dirs="etc"
    fpm_opts="--depends signal-cli"
    fpm_opts+=" --depends systemd"
    fpm_opts+=" --deb-systemd ${CI_PROJECT_DIR}/.packaging/signal-cli.service"
    ;;
esac
if [ -n "${signal_cli_branch}" ]; then
    tag="$(git -C /tmp/signal-cli describe --tags --dirty)"
    version="${tag/[a-zA-Z]/}+${signal_cli_branch/@/}"
    output="${CI_PROJECT_DIR}/${CI_JOB_NAME}-testing"
else
    version="${version//[a-zA-Z]/}"
    output="${CI_PROJECT_DIR}/${CI_JOB_NAME}"
fi
mkdir -p "${output}"
chmod +t /tmp
# shellcheck disable=SC2086
fpm \
    --input-type dir \
    --output-type deb \
    --force \
    --name "${NAME}" \
    --package "${output}/${NAME}_${version}+${patchlevel}_${architecture}.deb" \
    --architecture "${architecture}" \
    --version "${version}+${patchlevel}" \
    --maintainer "Stefan Heitmüller <stefan.heitmueller@gmx.com>" \
    --url "https://github.com/AsamK/signal-cli/" \
    --description "${description}" \
    --deb-recommends morph027-keyring \
    --prefix "/" \
    --chdir "${tmpdir}" \
    --before-install "${CI_PROJECT_DIR}/.packaging/before-install.sh" \
    ${fpm_opts} \
    ${fpm_source_dirs}

# test install
case "${CI_JOB_NAME}" in
    "signal-cli-native"*)
        apt-get -y install "${output}/${NAME}_${version}+${patchlevel}_${architecture}.deb"
        set +e
        out="$(signal-cli --service-environment sandbox -a "+49341123456" --verbose register --voice 2>&1)"
        set -e
        if ! grep -qE "Captcha|RateLimitException" <<<"$out"; then
            echo "${out}"
            printf "\e[1;32mERROR: unable to run signal-cli\e[0m\n"
            exit 1
        fi
        ;;
esac
