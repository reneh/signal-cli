+++
title = "Goodies"
weight = 6
+++

Ideas for tiny extensions based on this project:

* [JSON-RPC Bot](/signal-cli/goodies/bot/)
