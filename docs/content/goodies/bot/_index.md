+++
title = "Interactive Bot"
weight = 2
+++

## Signal Bot

Now as your monitoring solution is urging you with important messages, you probably want to respond and trigger certain actions or just fetch a cute animated cat to change your mood for the better.
