+++
title = "Installation"
weight = 1
+++

* [Standalone]({{< ref "/installation/standalone" >}} "Standalone")
* [Docker]({{< ref "/installation/docker" >}} "Docker")
