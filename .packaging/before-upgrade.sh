if [ -f /etc/default/signal-cli-backup ]; then
    if type sqlite3 >/dev/null 2>&1; then
        . /etc/default/signal-cli-backup
        if [ -n "${SIGNAL_CLI_USER}" ] && [ -n "${SIGNAL_CLI_DATA}" ]; then
            for file in "${SIGNAL_CLI_DATA}"/*/account.db
            do
                echo "signal-cli: creating backup of ${file}"
                sudo -u "${SIGNAL_CLI_USER}" -H sqlite3 "${file}" ".backup ${file}.$(date +%Y%m%d%H%M)"
            done
            exit 0
        fi
    fi
fi
echo "signal-cli: skipping backup of account databases(s)."
