if type systemctl >/dev/null 2>&1; then
  if systemctl is-active dbus; then
    systemctl reload dbus.service
  fi
fi
